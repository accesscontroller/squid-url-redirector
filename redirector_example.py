#!/usr/bin/python3

with open('out.log', 'a') as f:
    while True:
        req = input()

        parts = req.split(' ')

        conc_id = parts[0]

        f.write('{}\n'.format(';'.join(parts)))

        print('{} '.format(conc_id))
