NAME   := access-controller/squid-url-redirector
TAG    := $$(git log -1 --format=%h)
IMG    := ${NAME}:${TAG}
LATEST := ${NAME}:latest

GOPROXY := ${GOPROXY}

help:
	@echo "Hello!"

make-dirs:
	@mkdir -p build/

build: make-dirs
	@go build -o build/redirector
	@cp -r stub/static build/

build-docker-dev:
	@docker build -t ${IMG}-dev -f Dockerfile-dev --build-arg=GOPROXY=${GOPROXY} .
	@docker tag ${IMG}-dev ${LATEST}-dev

prepare-docker-volume:
	@mkdir -p docker/
	
	@cp squid.conf docker/
	@cp config/config.yml docker/

	@mkdir -p docker/certs/
	@openssl req -newkey rsa:2048 -nodes \
		-keyout docker/certs/ca.key -days 365 -out docker/certs/ca.crt \
		-x509 -subj /CN=router.dev.local/

run-docker-dev: build-docker-dev prepare-docker-volume
	@docker run -v docker:/mnt/squid/ ${LATEST}-dev