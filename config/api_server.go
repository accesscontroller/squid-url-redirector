package config

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/url"

	"github.com/spf13/viper"
)

// APIServer defines a configuration to connect to API server.
type APIServer struct {
	Server    url.URL
	AccessKey string

	CACerts     *x509.CertPool
	ClientCerts *tls.Certificate
}

func (s *APIServer) load(vp *viper.Viper) error {
	// APIServer URL.
	rawAPIServer := vp.GetString("api_server.server")
	if rawAPIServer == "" {
		return fmt.Errorf("%w: api_server.server value is not set", ErrLoadConfig)
	}

	APIServer, err := url.Parse(rawAPIServer)
	if err != nil {
		return fmt.Errorf("%w: api_server.server value parse error: %s", ErrLoadConfig, err)
	}

	s.Server = *APIServer

	// APIServerKey.
	s.AccessKey = vp.GetString("api_server.access_key")

	// CA Certificate.
	rawCACert := vp.GetString("api_server.ca_certs")
	if rawCACert != "" {
		// Parse.
		pool := x509.NewCertPool()

		if !pool.AppendCertsFromPEM([]byte(rawCACert)) {
			return fmt.Errorf("%w: api_server.ca_certs can not be parsed and loaded", ErrLoadConfig)
		}

		s.CACerts = pool
	}

	rawClientCert := vp.GetString("api_server.client.certificate")
	rawClientKey := vp.GetString("api_server.client.key")

	if rawClientCert != "" && rawClientKey != "" {
		crt, err := tls.X509KeyPair([]byte(rawClientCert), []byte(rawClientKey))
		if err != nil {
			return fmt.Errorf("%w: api_server.client can not be parsed as a valid TLS KeyPair, error: %s",
				ErrLoadConfig, err)
		}

		s.ClientCerts = &crt
	}

	return nil
}
