package config

import (
	"crypto/tls"
	"crypto/x509"
	"net/url"
	"os"
)

func parseURLPanic(s string) url.URL {
	u, err := url.Parse(s)
	if err != nil {
		panic(err)
	}

	return *u
}

func parseKeyPairPanic(crt, key string) *tls.Certificate {
	res, err := tls.X509KeyPair([]byte(crt), []byte(key))
	if err != nil {
		panic(err)
	}

	return &res
}

func parseX509CertPoolPanic(data string) *x509.CertPool {
	pool := x509.NewCertPool()

	if !pool.AppendCertsFromPEM([]byte(data)) {
		panic("Can not load x509.CertPool#AppendCertsFromPEM")
	}

	return pool
}

func getOSHostnamePanic() string {
	v, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	return v
}
