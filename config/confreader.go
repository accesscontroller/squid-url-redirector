package config

import (
	"fmt"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// DefaultDBPath defines a default path to store local bbolt DB file.
const DefaultDBPath string = "db.bbolt"

// TimersConfig defines configuration for ConfReader#TimersConfig.
type TimersConfig struct {
	FullReload         time.Duration
	UsersGroupsReload  time.Duration
	WebResourcesReload time.Duration
	AccessGroupsReload time.Duration
	SessionsReload     time.Duration
}

func (tc *TimersConfig) load(vp *viper.Viper) error {
	// Full reload.
	frD := vp.GetDuration("conf_reader.timers.full_reload")
	if frD == 0 {
		return fmt.Errorf("%w: ConfReader#Timers#FullReload is zero", ErrLoadConfig)
	}

	tc.FullReload = frD

	// Users Groups Reload.
	ugrD := vp.GetDuration("conf_reader.timers.users_groups_reload")
	if ugrD == 0 {
		return fmt.Errorf("%w: ConfReader#Timers#UsersGroupsReload is zero", ErrLoadConfig)
	}

	tc.UsersGroupsReload = ugrD

	// Web Resources Reload.
	wrrD := vp.GetDuration("conf_reader.timers.web_resources_reload")
	if wrrD == 0 {
		return fmt.Errorf("%w: ConfReader#Timers#WebResourcesReload is zero", ErrLoadConfig)
	}

	tc.WebResourcesReload = wrrD

	// Access Groups Reload.
	agrD := vp.GetDuration("conf_reader.timers.access_groups_reload")
	if agrD == 0 {
		return fmt.Errorf("%w: ConfReader#Timers#AccessGroupsReload is zero", ErrLoadConfig)
	}

	tc.AccessGroupsReload = agrD

	// Sessions Reload.
	srD := vp.GetDuration("conf_reader.timers.sessions_reload")
	if srD == 0 {
		return fmt.Errorf("%w: ConfReader#Timers#SessionsReload is zero", ErrLoadConfig)
	}

	tc.SessionsReload = srD

	return nil
}

// ConfReader defines configuration for ConfReader.
type ConfReader struct {
	DBPath string

	UsersGroupsSources []storage.ResourceNameT
	SessionsSources    []storage.ResourceNameT

	Timers TimersConfig
}

func (cr *ConfReader) load(vp *viper.Viper) error {
	// DB Path.
	rawDBPath := vp.GetString("conf_reader.db_path")
	if rawDBPath == "" {
		rawDBPath = DefaultDBPath
	}

	cr.DBPath = rawDBPath

	// Users Groups Sources.
	UGS := vp.GetStringSlice("conf_reader.users_groups_sources")

	cr.UsersGroupsSources = make([]storage.ResourceNameT, 0, len(UGS))

	for i := range UGS {
		cr.UsersGroupsSources = append(cr.UsersGroupsSources, storage.ResourceNameT(UGS[i]))
	}

	// Sessions Sources.
	SS := vp.GetStringSlice("conf_reader.sessions_sources")

	cr.SessionsSources = make([]storage.ResourceNameT, 0, len(SS))

	for i := range SS {
		cr.SessionsSources = append(cr.SessionsSources, storage.ResourceNameT(SS[i]))
	}

	// Timers.
	return cr.Timers.load(vp)
}
