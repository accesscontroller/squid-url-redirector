package config

import (
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

type TestStateSuite struct {
	suite.Suite
}

func (ts *TestStateSuite) SetupTest() {
	// Clean EnvVars.
	for _, e := range os.Environ() {
		// Get Key.
		kv := strings.Split(e, "=")

		k := kv[0]

		if err := os.Unsetenv(k); err != nil {
			ts.FailNowf("Can not clean an ENV VAR", "Key=%q, Error: %s", k, err)
		}
	}
}

func (ts *TestStateSuite) TestParseConfigSuccess() {
	var tests = []*struct {
		name string

		configPath string

		expectedState Configuration
	}{
		{
			name:       "test config file 1",
			configPath: ".",
			expectedState: Configuration{
				FailOverPass: true,
				PassAll:      true,
				LogOutput:    "path-to-write-logs-to.log",
				LogLevel:     LogDebug,
				StubServer: StubServer{
					RedirectURL: "http://stub-server.com/",
				},
				ConfReader: ConfReader{
					DBPath:             "very-long/db/path/bbolt.db",
					SessionsSources:    []storage.ResourceNameT{},
					UsersGroupsSources: []storage.ResourceNameT{},
					Timers: TimersConfig{
						AccessGroupsReload: 360 * time.Second,
						FullReload:         1200 * time.Second,
						SessionsReload:     60 * time.Second,
						UsersGroupsReload:  120 * time.Second,
						WebResourcesReload: 240 * time.Second,
					},
				},
				APIServer: APIServer{
					Server:    parseURLPanic("http://server1.com/path"),
					AccessKey: "strong api server key",
					CACerts: parseX509CertPoolPanic(`
-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUevk+dpUrbjgaTvQkd9GeJOVOa3UwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MDgxNTQzMzNaFw0yMTA4
MDgxNTQzMzNaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCj7Do2hGHS8Tw7OgpaT+xwhHu7n+/6MYlNfcMgJDRl
wSyKEASwOX1xinjMFY4DpmumUyUpG9m2k2YIbT+SFTwBkyHUcN8j4TXqGvJMcJ6b
lvZ7cyzqeb1PHfqok9lZYmj2PGEI/z6b/PiG7ia2G9pasdznfthC1y3lHLNgIlPu
KQTNEmAzUX1xgVmeOVlJnLSHWV56dtwUL2GpuFRF4SjUfDV6H9l0yAwPBoylm6Di
Zaq3lJfUw75vDRSLvxGj8a8c3zMIgjSYHAHTjRX3xtQTroK77+j0SXMQFF3aeYVR
xWfShaM2bIwSJL8pGBEQBZLBUId4v9brwOJP/y60wS1DAgMBAAGjUzBRMB0GA1Ud
DgQWBBR6dXfUHxuUJrjOk+LDcRNadMkI8TAfBgNVHSMEGDAWgBR6dXfUHxuUJrjO
k+LDcRNadMkI8TAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBz
UK9XiWYB8YUDwnJ/g3JLD0Y4E7lNDJQcz1ssnj3WsdHXB2/CHz1s9B4iqswSbTVy
Af8ClFTofBGdaPgSFA3rL/kELPx3u+OJub/xqlwm9tWhWtO9qYnMDBgBu6OhYPCv
S0B5pBj93MCqf+Fpbx9vB6PWwyboSJCqFmarHtMHbffU4EOV9L4J/2Pvm0kq6oI9
1TopG4KCVnT84M/36rpYdsiCXSlS4yazOrfYWUxrzweToHt/bOEqTz9P20D+Q+tE
qnCgTXXybPs6N/Su2V7Zqh10Vjb868TNPdLhIaUoQzfIf9DtciMaS46CcbFV1nHh
6fa03zZZtifLNTHe6YKP
-----END CERTIFICATE-----

-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUfy/G6joixjTY/a9a6HFEnzZSyVcwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MDgxNTQzNTJaFw0yMTA4
MDgxNTQzNTJaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCvfjlOZEX9Rsi1y3YB3yVWuq2njl/xViGSy9fDjiYM
kW86VJlHcoPWIzCW4mfXAcT+O7O9GsSm6PCM8elP9ZaW777D5CXZKaeyNCvt8L5t
7XKmsM30xX0jUvzkiZwWD+frSiWuD/ilDl0OcacaUnc+I3BNePbZ0byKywGaQ8wz
qvTWELaTwTXMeztDy+fbHPiR6toLAPtx/oJo1rpAMMbaDXQwE8dS2m4DO9JQmx38
plk0J6U3NH+dWetyVp7xyHok9NTeJUDKZPH7ygff255xS3YflYljrKquY3Zu0Ssd
RH/Ot2/bD68kcNPQ0tuMtr6tm2XE2dMN5boI9/xjkvSfAgMBAAGjUzBRMB0GA1Ud
DgQWBBRj6AwPqfQJrlatAS4icKw0r9WPVTAfBgNVHSMEGDAWgBRj6AwPqfQJrlat
AS4icKw0r9WPVTAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAC
7VBVhWT47ef+KD9NmbTIaDdU/RE1gE5sR+2pcn45zAu4oWq0ZZJ07c9MYG9mN2fl
h+shtHrupF8cdVAYYr5VncRIkKY6GWRMfkTjDvkk9LZojKAUG8y/IicO8FoA9Q1J
VFD1rojyIuSjNfUOVJSQa3tU2rTSYcPsMy7aetSjjsuoQZ+opBsl0Z4Zm5npsTGG
Th6ExD9tNE2z9SqRs0ea/c9HdoQ+cLsGsmB9lfF7WBnCWD5DmLu4z+MKWUHA7A5/
BR5f0qv29xvE9wurzz/t733ezU85cu4KB3G1lXnTpOTl/0FJJtEKWzlxrjgJWSoY
xWgZ19OK4Z5g7LTx+GMU
-----END CERTIFICATE-----
				`),
					ClientCerts: parseKeyPairPanic(
						`
-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUS0kA7lnG0oxkHueVnmfZwITSo1cwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MTExMTMzMDNaFw0yMTA4
MTExMTMzMDNaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDK861H9H1jKnY9BzPJNZ+olX0GmtODKE+kIZSnegMx
UTEo6t7Ua9KClCkBHRrZW8TgzlKBXY+7J4SYQYX4lgaQJXkfIVaPdaFhJiSIwk4V
Xs3/4ebCiWIMIg96qBSNexghxgctCK3qQRAWGyfYrFVegcJiO+xYiFtWmIF2A+1X
iR3kiX3rmr/xmPmtfR1mhffFQ14YYFuY8kqy+Kbe8qhfpSUtbIY8Q+JAAZYJ07Tu
m8518gb/nYTgskaqlJP6VsViU3lN1b456s0lhtNApZEArap503GZj7ZdREcGnWzg
VkMme+fUH1ZwNlNCdpuOHsGG71W5kGEE8ieMhGrWBfAFAgMBAAGjUzBRMB0GA1Ud
DgQWBBS132GrdR5SNzMrYVPW4ZpvJma59DAfBgNVHSMEGDAWgBS132GrdR5SNzMr
YVPW4ZpvJma59DAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQCA
4cf+ghEnzmNZiArdLm46MIp6F2mtmZGSKq+ZgJ6+cKZZljszhAmxEa/5JRXZpKn2
GqwvnoBNlWkPU0eoy8g3qxxeUJxb2hqr2hAAoV7wHWKGLzhvGxXe5UrguOrNKd2U
1enVs8Z73aEQNwHOp6LYNAyUbQllKM9eBXG2rXaojoazSslooPD/Kp25YrneuVRl
+rbao9xD7jaSnC1sxcgI3ff1lteCjcIRzhVQn+oH3vuGkQOgCQHcbwIIZXh5Jeoo
QBrB04fHJlzXaNRgV7IXSqPhnsu5NpOZwrPytt8aeZzkx3mP0hlBIrBIVbuswjaF
zfibne4nrU0TNnrFlxlZ
-----END CERTIFICATE-----
			`, `
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDK861H9H1jKnY9
BzPJNZ+olX0GmtODKE+kIZSnegMxUTEo6t7Ua9KClCkBHRrZW8TgzlKBXY+7J4SY
QYX4lgaQJXkfIVaPdaFhJiSIwk4VXs3/4ebCiWIMIg96qBSNexghxgctCK3qQRAW
GyfYrFVegcJiO+xYiFtWmIF2A+1XiR3kiX3rmr/xmPmtfR1mhffFQ14YYFuY8kqy
+Kbe8qhfpSUtbIY8Q+JAAZYJ07Tum8518gb/nYTgskaqlJP6VsViU3lN1b456s0l
htNApZEArap503GZj7ZdREcGnWzgVkMme+fUH1ZwNlNCdpuOHsGG71W5kGEE8ieM
hGrWBfAFAgMBAAECggEATX8I3t4D9xbiuWEDrERWp+gVKq16MK3240STX/QePfmz
3Nz6o4BcjNIIak1z0CXTosgGBzHzzVJEtmLmxkp7TRWSD4oKZJNVQSRwdOxFJrHh
1WCsXgJZtypWYM7HslKbn2HfoZmb10Rq4RYTfBAqNQE1eY9KK3A9+G4ZmTfcrDyY
P1GSW8ehdT31b8KLb8DMexy0anSNyB8O7ToWZ88ytd1NVxJ+8lb18FX91HxwiwSP
VfuIlw+5U+PafPx7N4KIZTTqVW22r0zdYi4dZqkxJDtJnChqReLtaSydt3i+3xsr
nMJaWxm3FyUNAtoTeyG6DNRzzgr3CupQ+ZhBbJ+hDQKBgQDtOvFyYwS1L7Yv0THX
/Y4MooxSxCUpM7OIzmMtpf1zEOFJBWV7fytW1JuVmXFnclr9qJBZILqsIgviIZXp
VDlzTpGfBsw5A8kRyV8Cv0vhsPE8Yf9FY/aY41IAfen6hamWylV8bhp/BN7t1MKW
Lf5KYPTZO5TS4Pa6yvGQ7YaK/wKBgQDbAm5crsYhNogdeKgAgm2MWPOMRg2XEepC
TeYzTZXKM4sk4D80BHhNeajSJPellFfesvBeTsFz5LPu3R1QGvOIcNruNjgN7qP4
Zos7Tm5CRzsQ5TOPlqf3Oehia2Cxz5xO+7we7SSqEnlTpnJkxBcKvN9mz2dbDecR
lt9JsKxY+wKBgQCgP4bFzBlsAxTAbEGNM5v28P51EkY/9Mq8CR55wXzZc3ebCegp
ahfuiaOnhIVUaa0JWApaCn/Osxjv4dwQR3Kfb11y3ParNOFoAG0LwAkUJaBvN4tm
ZkY3tKa56lRZhFywnXDtorqHPtUQNv5S0DLML352F4dKg+hm+ZwAWFLgJQKBgQCR
kUQZhnFakgxWymOgMxDtQBJhIg7WYqn5+B0R7HerNic+E27u/CmaFUsIolmYTMk6
1GS/ykj65wBRu9ZH8CH+hkR+ILuRRxTlfgL9IFxY4U95I17qCZEK2gqDoCtdBEk4
lkmrpKPCBxVtbCyMnWlTUcaLZz+jXdI/9q/jIvLrEQKBgACe5MaJfoVKwrSRCty8
Gq337mR2GZnPPb57uxk5uxiltLfAXymZMLsjf15nJvqKakZVXlDbhxSeYSY4u4US
j4O7aHYmdo0Pbb43umu0B/G5cbC1S27XPdQW5yrhJ9v2/aCJu6msXmrQ5zqyfBpr
D8fFTVlZWX7e+ZzY8nW4w8Ol
-----END PRIVATE KEY-----
			`),
				},
			},
		},
	}

	for _, test := range tests {
		gotCfg, gotErr := Load(test.configPath)

		if !ts.NoErrorf(gotErr, "Must not return error", "test %q", test.name) {
			ts.FailNowf("Can not continue", "test %q", test.name)
		}

		ts.Equalf(test.expectedState, *gotCfg, "Unexpected result in test %q", test.name)
	}
}

func (ts *TestStateSuite) TestParseConfigCustomPathEnvVarSuccess() {
	var tests = []*struct {
		name          string
		customPath    string
		expectedState Configuration
	}{
		{
			name:       "test config file custom path 1",
			customPath: "test-config-subpath/",
			expectedState: Configuration{
				FailOverPass: true,
				PassAll:      true,
				LogOutput:    "path-to-write-logs-to.log-other-path",
				LogLevel:     LogDebug,
				StubServer: StubServer{
					RedirectURL: fmt.Sprintf("http://%s:8080/", getOSHostnamePanic()),
				},
				ConfReader: ConfReader{
					DBPath:             "very-long/db/path/bbolt.db",
					SessionsSources:    []storage.ResourceNameT{},
					UsersGroupsSources: []storage.ResourceNameT{},
					Timers: TimersConfig{
						AccessGroupsReload: 360 * time.Second,
						FullReload:         1201 * time.Second,
						SessionsReload:     61 * time.Second,
						UsersGroupsReload:  120 * time.Second,
						WebResourcesReload: 240 * time.Second,
					},
				},
				APIServer: APIServer{
					Server:    parseURLPanic("http://server1.com/path"),
					AccessKey: "strong api server key",
					CACerts: parseX509CertPoolPanic(`
-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUevk+dpUrbjgaTvQkd9GeJOVOa3UwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MDgxNTQzMzNaFw0yMTA4
MDgxNTQzMzNaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCj7Do2hGHS8Tw7OgpaT+xwhHu7n+/6MYlNfcMgJDRl
wSyKEASwOX1xinjMFY4DpmumUyUpG9m2k2YIbT+SFTwBkyHUcN8j4TXqGvJMcJ6b
lvZ7cyzqeb1PHfqok9lZYmj2PGEI/z6b/PiG7ia2G9pasdznfthC1y3lHLNgIlPu
KQTNEmAzUX1xgVmeOVlJnLSHWV56dtwUL2GpuFRF4SjUfDV6H9l0yAwPBoylm6Di
Zaq3lJfUw75vDRSLvxGj8a8c3zMIgjSYHAHTjRX3xtQTroK77+j0SXMQFF3aeYVR
xWfShaM2bIwSJL8pGBEQBZLBUId4v9brwOJP/y60wS1DAgMBAAGjUzBRMB0GA1Ud
DgQWBBR6dXfUHxuUJrjOk+LDcRNadMkI8TAfBgNVHSMEGDAWgBR6dXfUHxuUJrjO
k+LDcRNadMkI8TAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBz
UK9XiWYB8YUDwnJ/g3JLD0Y4E7lNDJQcz1ssnj3WsdHXB2/CHz1s9B4iqswSbTVy
Af8ClFTofBGdaPgSFA3rL/kELPx3u+OJub/xqlwm9tWhWtO9qYnMDBgBu6OhYPCv
S0B5pBj93MCqf+Fpbx9vB6PWwyboSJCqFmarHtMHbffU4EOV9L4J/2Pvm0kq6oI9
1TopG4KCVnT84M/36rpYdsiCXSlS4yazOrfYWUxrzweToHt/bOEqTz9P20D+Q+tE
qnCgTXXybPs6N/Su2V7Zqh10Vjb868TNPdLhIaUoQzfIf9DtciMaS46CcbFV1nHh
6fa03zZZtifLNTHe6YKP
-----END CERTIFICATE-----

-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUfy/G6joixjTY/a9a6HFEnzZSyVcwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MDgxNTQzNTJaFw0yMTA4
MDgxNTQzNTJaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCvfjlOZEX9Rsi1y3YB3yVWuq2njl/xViGSy9fDjiYM
kW86VJlHcoPWIzCW4mfXAcT+O7O9GsSm6PCM8elP9ZaW777D5CXZKaeyNCvt8L5t
7XKmsM30xX0jUvzkiZwWD+frSiWuD/ilDl0OcacaUnc+I3BNePbZ0byKywGaQ8wz
qvTWELaTwTXMeztDy+fbHPiR6toLAPtx/oJo1rpAMMbaDXQwE8dS2m4DO9JQmx38
plk0J6U3NH+dWetyVp7xyHok9NTeJUDKZPH7ygff255xS3YflYljrKquY3Zu0Ssd
RH/Ot2/bD68kcNPQ0tuMtr6tm2XE2dMN5boI9/xjkvSfAgMBAAGjUzBRMB0GA1Ud
DgQWBBRj6AwPqfQJrlatAS4icKw0r9WPVTAfBgNVHSMEGDAWgBRj6AwPqfQJrlat
AS4icKw0r9WPVTAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAC
7VBVhWT47ef+KD9NmbTIaDdU/RE1gE5sR+2pcn45zAu4oWq0ZZJ07c9MYG9mN2fl
h+shtHrupF8cdVAYYr5VncRIkKY6GWRMfkTjDvkk9LZojKAUG8y/IicO8FoA9Q1J
VFD1rojyIuSjNfUOVJSQa3tU2rTSYcPsMy7aetSjjsuoQZ+opBsl0Z4Zm5npsTGG
Th6ExD9tNE2z9SqRs0ea/c9HdoQ+cLsGsmB9lfF7WBnCWD5DmLu4z+MKWUHA7A5/
BR5f0qv29xvE9wurzz/t733ezU85cu4KB3G1lXnTpOTl/0FJJtEKWzlxrjgJWSoY
xWgZ19OK4Z5g7LTx+GMU
-----END CERTIFICATE-----
				`),
					ClientCerts: parseKeyPairPanic(
						`
-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUS0kA7lnG0oxkHueVnmfZwITSo1cwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MTExMTMzMDNaFw0yMTA4
MTExMTMzMDNaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDK861H9H1jKnY9BzPJNZ+olX0GmtODKE+kIZSnegMx
UTEo6t7Ua9KClCkBHRrZW8TgzlKBXY+7J4SYQYX4lgaQJXkfIVaPdaFhJiSIwk4V
Xs3/4ebCiWIMIg96qBSNexghxgctCK3qQRAWGyfYrFVegcJiO+xYiFtWmIF2A+1X
iR3kiX3rmr/xmPmtfR1mhffFQ14YYFuY8kqy+Kbe8qhfpSUtbIY8Q+JAAZYJ07Tu
m8518gb/nYTgskaqlJP6VsViU3lN1b456s0lhtNApZEArap503GZj7ZdREcGnWzg
VkMme+fUH1ZwNlNCdpuOHsGG71W5kGEE8ieMhGrWBfAFAgMBAAGjUzBRMB0GA1Ud
DgQWBBS132GrdR5SNzMrYVPW4ZpvJma59DAfBgNVHSMEGDAWgBS132GrdR5SNzMr
YVPW4ZpvJma59DAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQCA
4cf+ghEnzmNZiArdLm46MIp6F2mtmZGSKq+ZgJ6+cKZZljszhAmxEa/5JRXZpKn2
GqwvnoBNlWkPU0eoy8g3qxxeUJxb2hqr2hAAoV7wHWKGLzhvGxXe5UrguOrNKd2U
1enVs8Z73aEQNwHOp6LYNAyUbQllKM9eBXG2rXaojoazSslooPD/Kp25YrneuVRl
+rbao9xD7jaSnC1sxcgI3ff1lteCjcIRzhVQn+oH3vuGkQOgCQHcbwIIZXh5Jeoo
QBrB04fHJlzXaNRgV7IXSqPhnsu5NpOZwrPytt8aeZzkx3mP0hlBIrBIVbuswjaF
zfibne4nrU0TNnrFlxlZ
-----END CERTIFICATE-----
			`, `
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDK861H9H1jKnY9
BzPJNZ+olX0GmtODKE+kIZSnegMxUTEo6t7Ua9KClCkBHRrZW8TgzlKBXY+7J4SY
QYX4lgaQJXkfIVaPdaFhJiSIwk4VXs3/4ebCiWIMIg96qBSNexghxgctCK3qQRAW
GyfYrFVegcJiO+xYiFtWmIF2A+1XiR3kiX3rmr/xmPmtfR1mhffFQ14YYFuY8kqy
+Kbe8qhfpSUtbIY8Q+JAAZYJ07Tum8518gb/nYTgskaqlJP6VsViU3lN1b456s0l
htNApZEArap503GZj7ZdREcGnWzgVkMme+fUH1ZwNlNCdpuOHsGG71W5kGEE8ieM
hGrWBfAFAgMBAAECggEATX8I3t4D9xbiuWEDrERWp+gVKq16MK3240STX/QePfmz
3Nz6o4BcjNIIak1z0CXTosgGBzHzzVJEtmLmxkp7TRWSD4oKZJNVQSRwdOxFJrHh
1WCsXgJZtypWYM7HslKbn2HfoZmb10Rq4RYTfBAqNQE1eY9KK3A9+G4ZmTfcrDyY
P1GSW8ehdT31b8KLb8DMexy0anSNyB8O7ToWZ88ytd1NVxJ+8lb18FX91HxwiwSP
VfuIlw+5U+PafPx7N4KIZTTqVW22r0zdYi4dZqkxJDtJnChqReLtaSydt3i+3xsr
nMJaWxm3FyUNAtoTeyG6DNRzzgr3CupQ+ZhBbJ+hDQKBgQDtOvFyYwS1L7Yv0THX
/Y4MooxSxCUpM7OIzmMtpf1zEOFJBWV7fytW1JuVmXFnclr9qJBZILqsIgviIZXp
VDlzTpGfBsw5A8kRyV8Cv0vhsPE8Yf9FY/aY41IAfen6hamWylV8bhp/BN7t1MKW
Lf5KYPTZO5TS4Pa6yvGQ7YaK/wKBgQDbAm5crsYhNogdeKgAgm2MWPOMRg2XEepC
TeYzTZXKM4sk4D80BHhNeajSJPellFfesvBeTsFz5LPu3R1QGvOIcNruNjgN7qP4
Zos7Tm5CRzsQ5TOPlqf3Oehia2Cxz5xO+7we7SSqEnlTpnJkxBcKvN9mz2dbDecR
lt9JsKxY+wKBgQCgP4bFzBlsAxTAbEGNM5v28P51EkY/9Mq8CR55wXzZc3ebCegp
ahfuiaOnhIVUaa0JWApaCn/Osxjv4dwQR3Kfb11y3ParNOFoAG0LwAkUJaBvN4tm
ZkY3tKa56lRZhFywnXDtorqHPtUQNv5S0DLML352F4dKg+hm+ZwAWFLgJQKBgQCR
kUQZhnFakgxWymOgMxDtQBJhIg7WYqn5+B0R7HerNic+E27u/CmaFUsIolmYTMk6
1GS/ykj65wBRu9ZH8CH+hkR+ILuRRxTlfgL9IFxY4U95I17qCZEK2gqDoCtdBEk4
lkmrpKPCBxVtbCyMnWlTUcaLZz+jXdI/9q/jIvLrEQKBgACe5MaJfoVKwrSRCty8
Gq337mR2GZnPPb57uxk5uxiltLfAXymZMLsjf15nJvqKakZVXlDbhxSeYSY4u4US
j4O7aHYmdo0Pbb43umu0B/G5cbC1S27XPdQW5yrhJ9v2/aCJu6msXmrQ5zqyfBpr
D8fFTVlZWX7e+ZzY8nW4w8Ol
-----END PRIVATE KEY-----
			`),
				},
			},
		},
	}

	for _, test := range tests {
		gotCfg, gotErr := Load(test.customPath)

		if !ts.NoErrorf(gotErr, "Must not return error", "test %q", test.name) {
			ts.FailNowf("Can not continue", "test %q", test.name)
		}

		ts.Equalf(test.expectedState, *gotCfg, "Unexpected result in test %q", test.name)
	}
}

func (ts *TestStateSuite) TestParseEnvVarsSuccess() {
	var tests = []*struct {
		name    string
		envVars map[string]string

		expectedState Configuration
	}{
		{
			name: "test 1",
			envVars: map[string]string{
				ENVVarPrefix + "_PASS_ALL":              "true",
				ENVVarPrefix + "_FAIL_OVER_PASS":        "true",
				ENVVarPrefix + "_API_SERVER.SERVER":     "http://server1.com/path",
				ENVVarPrefix + "_API_SERVER.ACCESS_KEY": "strong api server key",
				ENVVarPrefix + "_API_SERVER.CA_CERTS": `
-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUevk+dpUrbjgaTvQkd9GeJOVOa3UwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MDgxNTQzMzNaFw0yMTA4
MDgxNTQzMzNaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCj7Do2hGHS8Tw7OgpaT+xwhHu7n+/6MYlNfcMgJDRl
wSyKEASwOX1xinjMFY4DpmumUyUpG9m2k2YIbT+SFTwBkyHUcN8j4TXqGvJMcJ6b
lvZ7cyzqeb1PHfqok9lZYmj2PGEI/z6b/PiG7ia2G9pasdznfthC1y3lHLNgIlPu
KQTNEmAzUX1xgVmeOVlJnLSHWV56dtwUL2GpuFRF4SjUfDV6H9l0yAwPBoylm6Di
Zaq3lJfUw75vDRSLvxGj8a8c3zMIgjSYHAHTjRX3xtQTroK77+j0SXMQFF3aeYVR
xWfShaM2bIwSJL8pGBEQBZLBUId4v9brwOJP/y60wS1DAgMBAAGjUzBRMB0GA1Ud
DgQWBBR6dXfUHxuUJrjOk+LDcRNadMkI8TAfBgNVHSMEGDAWgBR6dXfUHxuUJrjO
k+LDcRNadMkI8TAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBz
UK9XiWYB8YUDwnJ/g3JLD0Y4E7lNDJQcz1ssnj3WsdHXB2/CHz1s9B4iqswSbTVy
Af8ClFTofBGdaPgSFA3rL/kELPx3u+OJub/xqlwm9tWhWtO9qYnMDBgBu6OhYPCv
S0B5pBj93MCqf+Fpbx9vB6PWwyboSJCqFmarHtMHbffU4EOV9L4J/2Pvm0kq6oI9
1TopG4KCVnT84M/36rpYdsiCXSlS4yazOrfYWUxrzweToHt/bOEqTz9P20D+Q+tE
qnCgTXXybPs6N/Su2V7Zqh10Vjb868TNPdLhIaUoQzfIf9DtciMaS46CcbFV1nHh
6fa03zZZtifLNTHe6YKP
-----END CERTIFICATE-----

-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUfy/G6joixjTY/a9a6HFEnzZSyVcwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MDgxNTQzNTJaFw0yMTA4
MDgxNTQzNTJaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCvfjlOZEX9Rsi1y3YB3yVWuq2njl/xViGSy9fDjiYM
kW86VJlHcoPWIzCW4mfXAcT+O7O9GsSm6PCM8elP9ZaW777D5CXZKaeyNCvt8L5t
7XKmsM30xX0jUvzkiZwWD+frSiWuD/ilDl0OcacaUnc+I3BNePbZ0byKywGaQ8wz
qvTWELaTwTXMeztDy+fbHPiR6toLAPtx/oJo1rpAMMbaDXQwE8dS2m4DO9JQmx38
plk0J6U3NH+dWetyVp7xyHok9NTeJUDKZPH7ygff255xS3YflYljrKquY3Zu0Ssd
RH/Ot2/bD68kcNPQ0tuMtr6tm2XE2dMN5boI9/xjkvSfAgMBAAGjUzBRMB0GA1Ud
DgQWBBRj6AwPqfQJrlatAS4icKw0r9WPVTAfBgNVHSMEGDAWgBRj6AwPqfQJrlat
AS4icKw0r9WPVTAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAC
7VBVhWT47ef+KD9NmbTIaDdU/RE1gE5sR+2pcn45zAu4oWq0ZZJ07c9MYG9mN2fl
h+shtHrupF8cdVAYYr5VncRIkKY6GWRMfkTjDvkk9LZojKAUG8y/IicO8FoA9Q1J
VFD1rojyIuSjNfUOVJSQa3tU2rTSYcPsMy7aetSjjsuoQZ+opBsl0Z4Zm5npsTGG
Th6ExD9tNE2z9SqRs0ea/c9HdoQ+cLsGsmB9lfF7WBnCWD5DmLu4z+MKWUHA7A5/
BR5f0qv29xvE9wurzz/t733ezU85cu4KB3G1lXnTpOTl/0FJJtEKWzlxrjgJWSoY
xWgZ19OK4Z5g7LTx+GMU
-----END CERTIFICATE-----
				`,
				ENVVarPrefix + "_API_SERVER.CLIENT.CERTIFICATE": `
-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUS0kA7lnG0oxkHueVnmfZwITSo1cwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MTExMTMzMDNaFw0yMTA4
MTExMTMzMDNaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDK861H9H1jKnY9BzPJNZ+olX0GmtODKE+kIZSnegMx
UTEo6t7Ua9KClCkBHRrZW8TgzlKBXY+7J4SYQYX4lgaQJXkfIVaPdaFhJiSIwk4V
Xs3/4ebCiWIMIg96qBSNexghxgctCK3qQRAWGyfYrFVegcJiO+xYiFtWmIF2A+1X
iR3kiX3rmr/xmPmtfR1mhffFQ14YYFuY8kqy+Kbe8qhfpSUtbIY8Q+JAAZYJ07Tu
m8518gb/nYTgskaqlJP6VsViU3lN1b456s0lhtNApZEArap503GZj7ZdREcGnWzg
VkMme+fUH1ZwNlNCdpuOHsGG71W5kGEE8ieMhGrWBfAFAgMBAAGjUzBRMB0GA1Ud
DgQWBBS132GrdR5SNzMrYVPW4ZpvJma59DAfBgNVHSMEGDAWgBS132GrdR5SNzMr
YVPW4ZpvJma59DAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQCA
4cf+ghEnzmNZiArdLm46MIp6F2mtmZGSKq+ZgJ6+cKZZljszhAmxEa/5JRXZpKn2
GqwvnoBNlWkPU0eoy8g3qxxeUJxb2hqr2hAAoV7wHWKGLzhvGxXe5UrguOrNKd2U
1enVs8Z73aEQNwHOp6LYNAyUbQllKM9eBXG2rXaojoazSslooPD/Kp25YrneuVRl
+rbao9xD7jaSnC1sxcgI3ff1lteCjcIRzhVQn+oH3vuGkQOgCQHcbwIIZXh5Jeoo
QBrB04fHJlzXaNRgV7IXSqPhnsu5NpOZwrPytt8aeZzkx3mP0hlBIrBIVbuswjaF
zfibne4nrU0TNnrFlxlZ
-----END CERTIFICATE-----
				`,
				ENVVarPrefix + "_API_SERVER.CLIENT.KEY": `
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDK861H9H1jKnY9
BzPJNZ+olX0GmtODKE+kIZSnegMxUTEo6t7Ua9KClCkBHRrZW8TgzlKBXY+7J4SY
QYX4lgaQJXkfIVaPdaFhJiSIwk4VXs3/4ebCiWIMIg96qBSNexghxgctCK3qQRAW
GyfYrFVegcJiO+xYiFtWmIF2A+1XiR3kiX3rmr/xmPmtfR1mhffFQ14YYFuY8kqy
+Kbe8qhfpSUtbIY8Q+JAAZYJ07Tum8518gb/nYTgskaqlJP6VsViU3lN1b456s0l
htNApZEArap503GZj7ZdREcGnWzgVkMme+fUH1ZwNlNCdpuOHsGG71W5kGEE8ieM
hGrWBfAFAgMBAAECggEATX8I3t4D9xbiuWEDrERWp+gVKq16MK3240STX/QePfmz
3Nz6o4BcjNIIak1z0CXTosgGBzHzzVJEtmLmxkp7TRWSD4oKZJNVQSRwdOxFJrHh
1WCsXgJZtypWYM7HslKbn2HfoZmb10Rq4RYTfBAqNQE1eY9KK3A9+G4ZmTfcrDyY
P1GSW8ehdT31b8KLb8DMexy0anSNyB8O7ToWZ88ytd1NVxJ+8lb18FX91HxwiwSP
VfuIlw+5U+PafPx7N4KIZTTqVW22r0zdYi4dZqkxJDtJnChqReLtaSydt3i+3xsr
nMJaWxm3FyUNAtoTeyG6DNRzzgr3CupQ+ZhBbJ+hDQKBgQDtOvFyYwS1L7Yv0THX
/Y4MooxSxCUpM7OIzmMtpf1zEOFJBWV7fytW1JuVmXFnclr9qJBZILqsIgviIZXp
VDlzTpGfBsw5A8kRyV8Cv0vhsPE8Yf9FY/aY41IAfen6hamWylV8bhp/BN7t1MKW
Lf5KYPTZO5TS4Pa6yvGQ7YaK/wKBgQDbAm5crsYhNogdeKgAgm2MWPOMRg2XEepC
TeYzTZXKM4sk4D80BHhNeajSJPellFfesvBeTsFz5LPu3R1QGvOIcNruNjgN7qP4
Zos7Tm5CRzsQ5TOPlqf3Oehia2Cxz5xO+7we7SSqEnlTpnJkxBcKvN9mz2dbDecR
lt9JsKxY+wKBgQCgP4bFzBlsAxTAbEGNM5v28P51EkY/9Mq8CR55wXzZc3ebCegp
ahfuiaOnhIVUaa0JWApaCn/Osxjv4dwQR3Kfb11y3ParNOFoAG0LwAkUJaBvN4tm
ZkY3tKa56lRZhFywnXDtorqHPtUQNv5S0DLML352F4dKg+hm+ZwAWFLgJQKBgQCR
kUQZhnFakgxWymOgMxDtQBJhIg7WYqn5+B0R7HerNic+E27u/CmaFUsIolmYTMk6
1GS/ykj65wBRu9ZH8CH+hkR+ILuRRxTlfgL9IFxY4U95I17qCZEK2gqDoCtdBEk4
lkmrpKPCBxVtbCyMnWlTUcaLZz+jXdI/9q/jIvLrEQKBgACe5MaJfoVKwrSRCty8
Gq337mR2GZnPPb57uxk5uxiltLfAXymZMLsjf15nJvqKakZVXlDbhxSeYSY4u4US
j4O7aHYmdo0Pbb43umu0B/G5cbC1S27XPdQW5yrhJ9v2/aCJu6msXmrQ5zqyfBpr
D8fFTVlZWX7e+ZzY8nW4w8Ol
-----END PRIVATE KEY-----
				`,
				ENVVarPrefix + "_CONF_READER.DB_PATH":                     "very-long/db/path/bbolt.db",
				ENVVarPrefix + "_CONF_READER.SESSIONS_SOURCES":            `session-source-1 session-source-2`,
				ENVVarPrefix + "_CONF_READER.USERS_GROUPS_SOURCES":        `users-groups-source-1 users-groups-source-2`,
				ENVVarPrefix + "_CONF_READER.TIMERS.FULL_RELOAD":          "1200s",
				ENVVarPrefix + "_CONF_READER.TIMERS.USERS_GROUPS_RELOAD":  "120s",
				ENVVarPrefix + "_CONF_READER.TIMERS.WEB_RESOURCES_RELOAD": "240s",
				ENVVarPrefix + "_CONF_READER.TIMERS.ACCESS_GROUPS_RELOAD": "360s",
				ENVVarPrefix + "_CONF_READER.TIMERS.SESSIONS_RELOAD":      "60s",
				ENVVarPrefix + "_STUB_SERVER.ENABLED":                     "true",
				ENVVarPrefix + "_STUB_SERVER.BIND":                        ":32000",
				ENVVarPrefix + "_STUB_SERVER.STATIC_CONTENT_PATH":         "static-content-path/subpath/",
				ENVVarPrefix + "_LOG_OUTPUT":                              "path-to-write-logs-to.log",
				ENVVarPrefix + "_LOG_LEVEL":                               "debug",
			},
			expectedState: Configuration{
				FailOverPass: true,
				PassAll:      true,
				LogOutput:    "path-to-write-logs-to.log",
				LogLevel:     LogDebug,
				StubServer: StubServer{
					RedirectURL: fmt.Sprintf("http://%s:8080/", getOSHostnamePanic()),
				},
				ConfReader: ConfReader{
					DBPath:             "very-long/db/path/bbolt.db",
					SessionsSources:    []storage.ResourceNameT{"session-source-1", "session-source-2"},
					UsersGroupsSources: []storage.ResourceNameT{"users-groups-source-1", "users-groups-source-2"},
					Timers: TimersConfig{
						AccessGroupsReload: 360 * time.Second,
						FullReload:         1200 * time.Second,
						SessionsReload:     60 * time.Second,
						UsersGroupsReload:  120 * time.Second,
						WebResourcesReload: 240 * time.Second,
					},
				},
				APIServer: APIServer{
					Server:    parseURLPanic("http://server1.com/path"),
					AccessKey: "strong api server key",
					CACerts: parseX509CertPoolPanic(`
-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUevk+dpUrbjgaTvQkd9GeJOVOa3UwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MDgxNTQzMzNaFw0yMTA4
MDgxNTQzMzNaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCj7Do2hGHS8Tw7OgpaT+xwhHu7n+/6MYlNfcMgJDRl
wSyKEASwOX1xinjMFY4DpmumUyUpG9m2k2YIbT+SFTwBkyHUcN8j4TXqGvJMcJ6b
lvZ7cyzqeb1PHfqok9lZYmj2PGEI/z6b/PiG7ia2G9pasdznfthC1y3lHLNgIlPu
KQTNEmAzUX1xgVmeOVlJnLSHWV56dtwUL2GpuFRF4SjUfDV6H9l0yAwPBoylm6Di
Zaq3lJfUw75vDRSLvxGj8a8c3zMIgjSYHAHTjRX3xtQTroK77+j0SXMQFF3aeYVR
xWfShaM2bIwSJL8pGBEQBZLBUId4v9brwOJP/y60wS1DAgMBAAGjUzBRMB0GA1Ud
DgQWBBR6dXfUHxuUJrjOk+LDcRNadMkI8TAfBgNVHSMEGDAWgBR6dXfUHxuUJrjO
k+LDcRNadMkI8TAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBz
UK9XiWYB8YUDwnJ/g3JLD0Y4E7lNDJQcz1ssnj3WsdHXB2/CHz1s9B4iqswSbTVy
Af8ClFTofBGdaPgSFA3rL/kELPx3u+OJub/xqlwm9tWhWtO9qYnMDBgBu6OhYPCv
S0B5pBj93MCqf+Fpbx9vB6PWwyboSJCqFmarHtMHbffU4EOV9L4J/2Pvm0kq6oI9
1TopG4KCVnT84M/36rpYdsiCXSlS4yazOrfYWUxrzweToHt/bOEqTz9P20D+Q+tE
qnCgTXXybPs6N/Su2V7Zqh10Vjb868TNPdLhIaUoQzfIf9DtciMaS46CcbFV1nHh
6fa03zZZtifLNTHe6YKP
-----END CERTIFICATE-----

-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUfy/G6joixjTY/a9a6HFEnzZSyVcwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MDgxNTQzNTJaFw0yMTA4
MDgxNTQzNTJaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCvfjlOZEX9Rsi1y3YB3yVWuq2njl/xViGSy9fDjiYM
kW86VJlHcoPWIzCW4mfXAcT+O7O9GsSm6PCM8elP9ZaW777D5CXZKaeyNCvt8L5t
7XKmsM30xX0jUvzkiZwWD+frSiWuD/ilDl0OcacaUnc+I3BNePbZ0byKywGaQ8wz
qvTWELaTwTXMeztDy+fbHPiR6toLAPtx/oJo1rpAMMbaDXQwE8dS2m4DO9JQmx38
plk0J6U3NH+dWetyVp7xyHok9NTeJUDKZPH7ygff255xS3YflYljrKquY3Zu0Ssd
RH/Ot2/bD68kcNPQ0tuMtr6tm2XE2dMN5boI9/xjkvSfAgMBAAGjUzBRMB0GA1Ud
DgQWBBRj6AwPqfQJrlatAS4icKw0r9WPVTAfBgNVHSMEGDAWgBRj6AwPqfQJrlat
AS4icKw0r9WPVTAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAC
7VBVhWT47ef+KD9NmbTIaDdU/RE1gE5sR+2pcn45zAu4oWq0ZZJ07c9MYG9mN2fl
h+shtHrupF8cdVAYYr5VncRIkKY6GWRMfkTjDvkk9LZojKAUG8y/IicO8FoA9Q1J
VFD1rojyIuSjNfUOVJSQa3tU2rTSYcPsMy7aetSjjsuoQZ+opBsl0Z4Zm5npsTGG
Th6ExD9tNE2z9SqRs0ea/c9HdoQ+cLsGsmB9lfF7WBnCWD5DmLu4z+MKWUHA7A5/
BR5f0qv29xvE9wurzz/t733ezU85cu4KB3G1lXnTpOTl/0FJJtEKWzlxrjgJWSoY
xWgZ19OK4Z5g7LTx+GMU
-----END CERTIFICATE-----
					`),
					ClientCerts: parseKeyPairPanic(
						`
-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUS0kA7lnG0oxkHueVnmfZwITSo1cwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMDA4MTExMTMzMDNaFw0yMTA4
MTExMTMzMDNaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDK861H9H1jKnY9BzPJNZ+olX0GmtODKE+kIZSnegMx
UTEo6t7Ua9KClCkBHRrZW8TgzlKBXY+7J4SYQYX4lgaQJXkfIVaPdaFhJiSIwk4V
Xs3/4ebCiWIMIg96qBSNexghxgctCK3qQRAWGyfYrFVegcJiO+xYiFtWmIF2A+1X
iR3kiX3rmr/xmPmtfR1mhffFQ14YYFuY8kqy+Kbe8qhfpSUtbIY8Q+JAAZYJ07Tu
m8518gb/nYTgskaqlJP6VsViU3lN1b456s0lhtNApZEArap503GZj7ZdREcGnWzg
VkMme+fUH1ZwNlNCdpuOHsGG71W5kGEE8ieMhGrWBfAFAgMBAAGjUzBRMB0GA1Ud
DgQWBBS132GrdR5SNzMrYVPW4ZpvJma59DAfBgNVHSMEGDAWgBS132GrdR5SNzMr
YVPW4ZpvJma59DAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQCA
4cf+ghEnzmNZiArdLm46MIp6F2mtmZGSKq+ZgJ6+cKZZljszhAmxEa/5JRXZpKn2
GqwvnoBNlWkPU0eoy8g3qxxeUJxb2hqr2hAAoV7wHWKGLzhvGxXe5UrguOrNKd2U
1enVs8Z73aEQNwHOp6LYNAyUbQllKM9eBXG2rXaojoazSslooPD/Kp25YrneuVRl
+rbao9xD7jaSnC1sxcgI3ff1lteCjcIRzhVQn+oH3vuGkQOgCQHcbwIIZXh5Jeoo
QBrB04fHJlzXaNRgV7IXSqPhnsu5NpOZwrPytt8aeZzkx3mP0hlBIrBIVbuswjaF
zfibne4nrU0TNnrFlxlZ
-----END CERTIFICATE-----
				`, `
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDK861H9H1jKnY9
BzPJNZ+olX0GmtODKE+kIZSnegMxUTEo6t7Ua9KClCkBHRrZW8TgzlKBXY+7J4SY
QYX4lgaQJXkfIVaPdaFhJiSIwk4VXs3/4ebCiWIMIg96qBSNexghxgctCK3qQRAW
GyfYrFVegcJiO+xYiFtWmIF2A+1XiR3kiX3rmr/xmPmtfR1mhffFQ14YYFuY8kqy
+Kbe8qhfpSUtbIY8Q+JAAZYJ07Tum8518gb/nYTgskaqlJP6VsViU3lN1b456s0l
htNApZEArap503GZj7ZdREcGnWzgVkMme+fUH1ZwNlNCdpuOHsGG71W5kGEE8ieM
hGrWBfAFAgMBAAECggEATX8I3t4D9xbiuWEDrERWp+gVKq16MK3240STX/QePfmz
3Nz6o4BcjNIIak1z0CXTosgGBzHzzVJEtmLmxkp7TRWSD4oKZJNVQSRwdOxFJrHh
1WCsXgJZtypWYM7HslKbn2HfoZmb10Rq4RYTfBAqNQE1eY9KK3A9+G4ZmTfcrDyY
P1GSW8ehdT31b8KLb8DMexy0anSNyB8O7ToWZ88ytd1NVxJ+8lb18FX91HxwiwSP
VfuIlw+5U+PafPx7N4KIZTTqVW22r0zdYi4dZqkxJDtJnChqReLtaSydt3i+3xsr
nMJaWxm3FyUNAtoTeyG6DNRzzgr3CupQ+ZhBbJ+hDQKBgQDtOvFyYwS1L7Yv0THX
/Y4MooxSxCUpM7OIzmMtpf1zEOFJBWV7fytW1JuVmXFnclr9qJBZILqsIgviIZXp
VDlzTpGfBsw5A8kRyV8Cv0vhsPE8Yf9FY/aY41IAfen6hamWylV8bhp/BN7t1MKW
Lf5KYPTZO5TS4Pa6yvGQ7YaK/wKBgQDbAm5crsYhNogdeKgAgm2MWPOMRg2XEepC
TeYzTZXKM4sk4D80BHhNeajSJPellFfesvBeTsFz5LPu3R1QGvOIcNruNjgN7qP4
Zos7Tm5CRzsQ5TOPlqf3Oehia2Cxz5xO+7we7SSqEnlTpnJkxBcKvN9mz2dbDecR
lt9JsKxY+wKBgQCgP4bFzBlsAxTAbEGNM5v28P51EkY/9Mq8CR55wXzZc3ebCegp
ahfuiaOnhIVUaa0JWApaCn/Osxjv4dwQR3Kfb11y3ParNOFoAG0LwAkUJaBvN4tm
ZkY3tKa56lRZhFywnXDtorqHPtUQNv5S0DLML352F4dKg+hm+ZwAWFLgJQKBgQCR
kUQZhnFakgxWymOgMxDtQBJhIg7WYqn5+B0R7HerNic+E27u/CmaFUsIolmYTMk6
1GS/ykj65wBRu9ZH8CH+hkR+ILuRRxTlfgL9IFxY4U95I17qCZEK2gqDoCtdBEk4
lkmrpKPCBxVtbCyMnWlTUcaLZz+jXdI/9q/jIvLrEQKBgACe5MaJfoVKwrSRCty8
Gq337mR2GZnPPb57uxk5uxiltLfAXymZMLsjf15nJvqKakZVXlDbhxSeYSY4u4US
j4O7aHYmdo0Pbb43umu0B/G5cbC1S27XPdQW5yrhJ9v2/aCJu6msXmrQ5zqyfBpr
D8fFTVlZWX7e+ZzY8nW4w8Ol
-----END PRIVATE KEY-----
				`),
				},
			},
		},
	}

	for _, test := range tests {
		// Set EnvVars.
		for k, v := range test.envVars {
			if err := os.Setenv(k, v); err != nil {
				ts.FailNowf("Can not os.Setenv",
					"test: %q, Key=%q, Value=%q, Error: %s", test.name, k, v, err)
			}
		}

		gotCfg, gotErr := Load()

		if !ts.NoErrorf(gotErr, "Must not return error", "test %q", test.name) {
			ts.FailNowf("Can not continue", "test %q", test.name)
		}

		ts.Equalf(test.expectedState, *gotCfg, "Unexpected result in test %q", test.name)
	}
}

func TestState(t *testing.T) {
	suite.Run(t, &TestStateSuite{})
}
