module gitlab.com/accesscontroller/squid-url-redirector/config

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/spf13/afero v1.3.4 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20200814195844-71bde2d5f00d
	golang.org/x/sys v0.0.0-20200814200057-3d37ad5750ed // indirect
	gopkg.in/ini.v1 v1.58.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
