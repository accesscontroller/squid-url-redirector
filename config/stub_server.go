package config

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

// DefaultStaticContentPath defines a default value for Static content path.
const (
	DefaultStaticContentPath   string = "static"
	DefaultBindSocket          string = ":8080"
	DefaultStubServerLogOutput string = "Stdout"
)

// StubServer defines a stub (Squid redirect URL) server configuration.
type StubServer struct {
	// RedirectURL defines a URL at which the StubServer serves requests.
	// Default is http://{{ os.Hostname }}:8080//.
	RedirectURL string
}

func (s *StubServer) load(vp *viper.Viper) error {
	// Prefix URL.
	s.RedirectURL = vp.GetString("stub_server.redirect_url")
	if s.RedirectURL == "" {
		v, err := os.Hostname()
		if err != nil {
			return fmt.Errorf("can not build default StubServer#RedirectURL: %w", err)
		}

		s.RedirectURL = fmt.Sprintf("http://%s:8080/", v)
	}

	return nil
}
