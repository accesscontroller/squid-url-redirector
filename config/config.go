package config

import (
	"errors"
	"strings"

	"github.com/spf13/viper"
)

const (
	ENVVarPrefix   = "SQUID_URL_REDIRECTOR"
	ConfigFileName = "config"
)

type LogLevel string

const (
	LogDebug LogLevel = "DEBUG"
	LogInfo  LogLevel = "INFO"
)

var ErrLoadConfig = errors.New("can not load configuration")

// Configuration contains full redirector configuration.
type Configuration struct {
	ConfReader ConfReader

	APIServer APIServer

	StubServer StubServer

	PassAll      bool
	FailOverPass bool

	LogLevel  LogLevel
	LogOutput string
}

// Load loads a Configuration using Viper.
func Load(configPaths ...string) (*Configuration, error) {
	var st Configuration

	vp := viper.New()

	// Env Vars.
	vp.SetEnvPrefix(ENVVarPrefix)
	vp.AutomaticEnv()

	// Config file paths.
	for i := range configPaths {
		vp.AddConfigPath(configPaths[i])
	}

	vp.SetConfigName(ConfigFileName)

	if len(configPaths) > 0 {
		if err := vp.ReadInConfig(); err != nil {
			return nil, err
		}
	}

	// Parse.
	// API Server.
	if err := st.APIServer.load(vp); err != nil {
		return nil, err
	}

	// ConfReader.
	if err := st.ConfReader.load(vp); err != nil {
		return nil, err
	}

	// Stub Server.
	if err := st.StubServer.load(vp); err != nil {
		return nil, err
	}

	// PassAll.
	st.PassAll = vp.GetBool("pass_all")

	// FailOverPass.
	st.FailOverPass = vp.GetBool("fail_over_pass")

	// Log Output.
	st.LogOutput = vp.GetString("log_output")

	// Log Level.
	rawLL := vp.GetString("log_level")
	if LogLevel(strings.ToUpper(rawLL)) == LogDebug {
		st.LogLevel = LogDebug
	} else {
		st.LogLevel = LogInfo
	}

	return &st, nil
}
