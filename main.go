package main

import (
	"context"
	"flag"
	"log"
	"os"

	"gitlab.com/accesscontroller/cclient.v1"
	"gitlab.com/accesscontroller/confreader"
	"gitlab.com/accesscontroller/confreader/db"
	"gitlab.com/accesscontroller/squid-url-redirector/config"
	"gitlab.com/accesscontroller/squid-url-redirector/handler"
	"gitlab.com/accesscontroller/squid-url-redirector/stub"
	"go.uber.org/zap"
)

func parseConfigFilePath() string {
	path := flag.String("configPath", "", "Defines extra paths for configuration file lookup")

	flag.Parse()

	return *path
}

func initLogger(cfg *config.Configuration) (*zap.Logger, error) {
	logCfg := zap.NewProductionConfig()

	if cfg.LogOutput == "" {
		cfg.LogOutput = os.Stderr.Name()
	}

	logCfg.OutputPaths = []string{cfg.LogOutput}

	if cfg.LogLevel == config.LogDebug {
		logCfg.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	}

	logger, err := logCfg.Build(zap.Fields(
		zap.String("module", "main"),
	))
	if err != nil {
		return nil, err
	}

	return logger, nil
}

func initConfReader(cfg *config.Configuration, localStorage confreader.LocalStorage,
	client *cclient.CClient, logger *zap.Logger) *confreader.ConfReader {
	tmrs := confreader.TimersConfig(cfg.ConfReader.Timers)

	cfgReader := confreader.New(cfg.ConfReader.UsersGroupsSources, cfg.ConfReader.SessionsSources,
		client, localStorage, &tmrs, logger.With(zap.String("package", "confreader")))

	cfgReader.InitReloadLoops(context.Background())

	return cfgReader
}

func main() {
	// Configuration.
	cfg, err := config.Load(parseConfigFilePath())
	if err != nil {
		log.Fatalln("Can not load configuration", err)
	}

	// Logger.
	logger, err := initLogger(cfg)
	if err != nil {
		log.Fatalln(err)
	}

	defer logger.Sync()

	// Print configuration.
	if config.LogDebug == cfg.LogLevel {
		logger.Sugar().Debug(cfg)
	}

	// Init Local Storage.
	localStorage, err := db.New(cfg.ConfReader.DBPath)
	if err != nil {
		logger.Fatal("Can not init Local Storage", zap.Error(err))
	}

	// API Client.
	client, err := cclient.NewDefault(&cfg.APIServer.Server,
		cfg.APIServer.CACerts, cfg.APIServer.ClientCerts)
	if err != nil {
		logger.Fatal("Can not init API Server Client", zap.Error(err))
	}

	// Init Configuration Loader.
	cfgReader := initConfReader(cfg, localStorage, client, logger)

	// Redirect URL.
	redirectURLGen := stub.NewRedirectURLBuilder(cfg.StubServer.RedirectURL)

	// Init Squid Request Handler.
	hndlInst := handler.New(cfg.FailOverPass, cfgReader, redirectURLGen, logger.With(zap.String("package", "handler")))

	logger.Fatal("Stopped", zap.Error(hndlInst.ServeContext(context.Background(), os.Stdin, os.Stdout)))
}
