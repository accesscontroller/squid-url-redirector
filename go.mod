module gitlab.com/accesscontroller/squid-url-redirector

go 1.14

require (
	github.com/magiconair/properties v1.8.3 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/spf13/afero v1.4.0 // indirect
	gitlab.com/accesscontroller/cclient.v1 v0.0.0-20200915184932-6b79b4bf5fec
	gitlab.com/accesscontroller/confreader v0.0.0-20200919124612-98bd190c2adf
	gitlab.com/accesscontroller/confreader/db v0.0.0-20200919124612-98bd190c2adf
	gitlab.com/accesscontroller/confreader/types v0.0.0-20200919124612-98bd190c2adf // indirect
	gitlab.com/accesscontroller/squid-url-redirector/config v0.0.0-20200920134916-4ca70a538033
	gitlab.com/accesscontroller/squid-url-redirector/handler v0.0.0-20200920134916-4ca70a538033
	gitlab.com/accesscontroller/squid-url-redirector/stub v0.0.0-20200920134916-4ca70a538033
	go.uber.org/zap v1.16.0
	gopkg.in/ini.v1 v1.61.0 // indirect
)
