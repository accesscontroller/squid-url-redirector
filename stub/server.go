package stub

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"path"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// IndexName defines a name of index page template.
const IndexName = "index.tmpl"

// Server serves Squid access denied page.
type Server struct {
	e *echo.Echo
}

// New creates a new Server where path defines static root directory.
// If path is empty, then uses "./static".
func New(staticPath string, logOut io.Writer) (*Server, error) {
	if staticPath == "" {
		staticPath = "static"
	}

	// Router.
	e := echo.New()

	logCfg := middleware.DefaultLoggerConfig

	logCfg.Output = logOut

	e.Use(middleware.LoggerWithConfig(logCfg), middleware.Recover())

	// Static files.
	e.Static("/static", staticPath)

	// BlockPage.
	// Load Template.
	tmpl, err := template.ParseFiles(path.Join(staticPath, IndexName))
	if err != nil {
		return nil, fmt.Errorf("can not parse Index Template: %w", err)
	}

	e.GET("/", createServeBlockPage(tmpl))

	return &Server{
		e: e,
	}, nil
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.e.ServeHTTP(w, r)
}

func createServeBlockPage(indexTemplate *template.Template) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Load request data.
		var paramValues struct {
			ClientIP            string
			ClientName          string
			AccessGroup         string
			DenyMessage         string
			WebResourceName     string
			WebResourceCategory string
			ErrorMessage        string
			RequestURL          string
		}

		paramValues.AccessGroup = c.QueryParam(paramAccessGroup)
		paramValues.ClientIP = c.QueryParam(paramClientIP)
		paramValues.ClientName = c.QueryParam(paramClientName)
		paramValues.DenyMessage = c.QueryParam(paramDenyMessage)
		paramValues.WebResourceName = c.QueryParam(paramWebResourceName)
		paramValues.WebResourceCategory = c.QueryParam(paramWebResourceCategory)
		paramValues.ErrorMessage = c.QueryParam(paramErrorMessage)
		paramValues.RequestURL = c.QueryParam(paramRequestURL)

		// Render.
		var bb bytes.Buffer

		if err := indexTemplate.Execute(&bb, paramValues); err != nil {
			return echo.ErrInternalServerError.SetInternal(err)
		}

		return c.HTMLBlob(http.StatusOK, bb.Bytes())
	}
}
