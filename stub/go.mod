module gitlab.com/accesscontroller/squid-url-redirector/stub

go 1.14

require (
	github.com/kr/text v0.2.0 // indirect
	github.com/labstack/echo/v4 v4.1.17
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20200913103725-48995d0f8362 // indirect
	gitlab.com/accesscontroller/confreader/types v0.0.0-20200919124612-98bd190c2adf
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
	golang.org/x/sys v0.0.0-20200918174421-af09f7315aff // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
)
