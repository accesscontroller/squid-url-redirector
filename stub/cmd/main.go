package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/accesscontroller/squid-url-redirector/stub"
)

const path = "../static"

type flags struct {
	isHelp bool

	bind string
	path string
}

func (f *flags) printHelp() {
	flag.Usage()
}

func (f *flags) parse() {
	flag.StringVar(&f.bind, "bind", ":8080", "Port to bind server to.")
	flag.StringVar(&f.path, "staticpath", path, "Defines static content path.")

	flag.Parse()
}

func main() {
	// Flags.
	var fl = &flags{}

	fl.parse()

	// Help.
	if fl.isHelp {
		fl.printHelp()

		return
	}

	// Print current run parameters.
	fmt.Printf("Server is starting.\nServer will listen on %q port and use %q path for templates.\n"+
		"To change start parameters call the program with -help.\n",
		fl.bind, fl.path)

	srv, err := stub.New(fl.path, os.Stdout)
	if err != nil {
		log.Fatalln(err)
	}

	log.Fatalln(http.ListenAndServe(fl.bind, srv))
}
