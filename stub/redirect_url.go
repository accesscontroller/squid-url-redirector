package stub

import (
	"net"
	"net/url"

	"gitlab.com/accesscontroller/confreader/types"
)

const (
	paramClientIP            = "client_ip"
	paramClientName          = "client_name"
	paramAccessGroup         = "access_group"
	paramDenyMessage         = "deny_message"
	paramWebResourceName     = "web_resource"
	paramWebResourceCategory = "web_resource_category"
	paramErrorMessage        = "error_message"
	paramRequestURL          = "request_url"
)

// RedirectURLBuilder makes redirect URLs.
type RedirectURLBuilder struct {
	prefix string
}

// NewRedirectURLBuilder creates a new RedirectURL builder.
func NewRedirectURLBuilder(prefix string) *RedirectURLBuilder {
	return &RedirectURLBuilder{
		prefix: prefix + "?",
	}
}

// MakeRedirectURL generates a url to redirect to.
func (r *RedirectURLBuilder) MakeRedirectURL(res types.CheckAccessResultGetter, clientIP net.IP) string { // nolint:interfacer
	var vals = &url.Values{}

	uName, uSource := res.GetUserInfo()
	accG := res.GetAccessGroup()
	webRes, category := res.GetWebResourceInfo()

	vals.Add(paramClientIP, clientIP.String())
	vals.Add(paramClientName, string(uName+"@"+uSource))
	vals.Add(paramAccessGroup, string(accG.Metadata.Name))
	vals.Add(paramDenyMessage, string(accG.Data.ExtraAccessDenyMessage))
	vals.Add(paramWebResourceName, string(webRes))
	vals.Add(paramWebResourceCategory, string(category))

	return r.prefix + vals.Encode()
}

// MakeErrRedirectURL generates a redirect URL in case of error.
func (r *RedirectURLBuilder) MakeErrRedirectURL(_URL string, clientIP net.IP, err error) string { // nolint:interfacer
	var vals = &url.Values{}

	vals.Add(paramErrorMessage, err.Error())
	vals.Add(paramClientIP, clientIP.String())
	vals.Add(paramRequestURL, _URL)

	return r.prefix + vals.Encode()
}
