module gitlab.com/accesscontroller/squid-url-redirector/handler

go 1.14

require (
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.6.1
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20200913103725-48995d0f8362
	gitlab.com/accesscontroller/confreader/types v0.0.0-20200816053952-fa7ce30259cd
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
)
