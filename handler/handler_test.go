package handler

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/confreader/types"
	"go.uber.org/zap"
)

type TestHandleRequestSuite struct {
	suite.Suite
}

func (ts *TestHandleRequestSuite) TestHandleRequestSuccess() {
	var tests = []*struct {
		name string

		concurrencyID int
		failPass      bool

		// Access Check Result.
		accessGroup storage.ResourceNameT
		isAllowed   bool
		user        storage.ResourceNameT
		source      storage.ResourceNameT

		in     string
		out    *bytes.Buffer
		mkr    *TestMockRedirectURLMaker
		logger *zap.Logger

		expectedError error
	}{
		{
			concurrencyID: 1001,
			accessGroup:   "access group 1",
			isAllowed:     true,
			user:          "user-1",
			source:        "source-1",
			in:            "1001 http://ya.ru/path 192.168.0.1/192.168.0.1 - CONNECT myip=- myport=3130",
			out:           &bytes.Buffer{},
			expectedError: nil,
			logger:        zap.NewExample(),
			mkr:           &TestMockRedirectURLMaker{},
			name:          "test 1 - URL Access Allowed",
		},
	}

	for _, test := range tests {
		// Make Mock.
		chM := &TestMockCheckAccessResult{}

		chM.On("IsAllowed").Return(test.isAllowed)
		chM.On("GetAccessGroup").Return(&storage.AccessGroupV1{
			Metadata: storage.MetadataV1{
				Name: test.accessGroup,
			},
		})
		chM.On("GetUserInfo").Return(storage.ResourceNameT("user1"), storage.ResourceNameT("source1"))
		chM.On("GetWebResourceInfo").Return(storage.ResourceNameT("web resource 1"), storage.ResourceNameT("web category 1"))

		gotErr := handleRequest(test.in, test.out, test.failPass,
			func(req *SquidRequest) (types.CheckAccessResultGetter, error) {
				return chM, nil
			}, test.mkr, test.logger)

		if !ts.NoErrorf(gotErr, "test %s", test.name) {
			ts.FailNow("Can not continue")
		}

		ts.Equal(fmt.Sprintf("%d OK access_group=%q\n",
			test.concurrencyID, test.accessGroup),
			test.out.String())

		chM.AssertExpectations(ts.T())
	}
}

func (ts *TestHandleRequestSuite) TestHandleRequestCheckError() {
	var chErr = errors.New("check access error") // nolint:goerr113

	var tests = []*struct {
		name string

		concurrencyID int
		failPass      bool

		// Access Check Result.
		accessGroup string
		isAllowed   bool
		user        storage.ResourceNameT
		source      storage.ResourceNameT

		in     string
		out    *bytes.Buffer
		mkr    *TestMockRedirectURLMaker
		logger *zap.Logger

		// Mock URL Redirect maker config.
		inURL       string
		clientIP    net.IP
		outRedirURL string
	}{
		{
			concurrencyID: 1001,
			accessGroup:   "access group 1",
			isAllowed:     true,
			user:          "user-1",
			source:        "source-1",
			in:            "1001 http://ya.ru/path 192.168.0.1/192.168.0.1 - CONNECT myip=- myport=3130",
			out:           &bytes.Buffer{},
			logger:        zap.NewExample(),
			mkr:           &TestMockRedirectURLMaker{},
			name:          "test 1 - URL Check Access error",
			inURL:         "http://ya.ru/path",
			clientIP:      net.ParseIP("192.168.0.1"),
			outRedirURL:   "http://redirected-to/",
		},
	}

	for _, test := range tests {
		tm := &TestMockCheckAccessResult{}

		test.mkr.On("MakeErrRedirectURL", test.inURL, test.clientIP, chErr).Return(test.outRedirURL)

		gotErr := handleRequest(test.in, test.out,
			test.failPass,
			func(req *SquidRequest) (types.CheckAccessResultGetter, error) {
				return tm, chErr
			}, test.mkr, test.logger)

		if !ts.NoErrorf(gotErr, "test %s", test.name) {
			ts.FailNow("Can not continue")
		}

		ts.Equal(fmt.Sprintf("%d OK message=%q 302:%q\n",
			test.concurrencyID, chErr, test.outRedirURL),
			test.out.String())

		tm.AssertExpectations(ts.T())
	}
}

func TestHandleRequest(t *testing.T) {
	suite.Run(t, &TestHandleRequestSuite{})
}
