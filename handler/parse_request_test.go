package handler

import (
	"net"
	"reflect"
	"testing"
)

func Test_parseRequest(t *testing.T) {
	type args struct {
		in string
	}

	tests := []struct {
		name    string
		args    args
		want    *SquidRequest
		wantErr bool
	}{
		{
			name: "test 1",
			args: args{
				in: "123 arc.msn.com:443 192.168.0.153/192.168.0.153 - CONNECT myip=- myport=3130\n",
			},
			want: &SquidRequest{
				ConcurencyID: 123,
				ClientFQDN:   "192.168.0.153",
				ClientIP:     net.ParseIP("192.168.0.153"),
				Method:       "CONNECT",
				MyPort:       3130,
				URL:          "arc.msn.com:443",
				Ident:        "",
				MyIP:         "-",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseRequest(tt.args.in) // nolint:scopelint
			if (err != nil) != tt.wantErr {      // nolint:scopelint
				t.Errorf("parseRequest() error = %v, wantErr %v", err, tt.wantErr) // nolint:scopelint

				return
			}
			if !reflect.DeepEqual(got, tt.want) { // nolint:scopelint
				t.Errorf("parseRequest() = %v, want %v", got, tt.want) // nolint:scopelint
			}
		})
	}
}
