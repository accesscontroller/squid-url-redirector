package handler

import "testing"

func Test_isIP(t *testing.T) {
	type args struct {
		raw string
	}

	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "IP: 10.8.0.1",
			args: args{
				raw: "10.8.0.1",
			},
			want: true,
		},
		{
			name: "IP: fe80::1",
			args: args{
				raw: "::1",
			},
			want: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isIP(tt.args.raw); got != tt.want { // nolint:scopelint
				t.Errorf("isIP() = %v, want %v", got, tt.want) // nolint:scopelint
			}
		})
	}
}
