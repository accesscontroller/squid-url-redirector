package handler

import (
	"fmt"
	"net"
	"net/url"
	"regexp"

	"gitlab.com/accesscontroller/confreader/types"
)

// Defines a Regex which matches all IPv4 and IPv6 addresses.
const ipv4v6Regex = `((?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3})|` +
	`((?:(?:(?:[0-9A-Fa-f]{0,4}:){7}[0-9A-Fa-f]{0,4})|(?:(?:[0-9A-Fa-f]{0,4}:){6}:[0-9A-Fa-f]{0,4})|` +
	`(?:(?:[0-9A-Fa-f]{0,4}:){5}:(?:[0-9A-Fa-f]{0,4}:)?[0-9A-Fa-f]{0,4})|` +
	`(?:(?:[0-9A-Fa-f]{0,4}:){4}:(?:[0-9A-Fa-f]{0,4}:){0,2}[0-9A-Fa-f]{0,4})|` +
	`(?:(?:[0-9A-Fa-f]{0,4}:){3}:(?:[0-9A-Fa-f]{0,4}:){0,3}[0-9A-Fa-f]{0,4})|` +
	`(?:(?:[0-9A-Fa-f]{0,4}:){2}:(?:[0-9A-Fa-f]{0,4}:){0,4}[0-9A-Fa-f]{0,4})|` +
	`(?:(?:[0-9A-Fa-f]{0,4}:){6}(?:(?:(?:25[0-5])|(?:2[0-4]\d)|` +
	`(?:1\d{2})|(?:\d{1,2}))\.){3}(?:(?:25[0-5])|(?:2[0-4]\d)|(?:1\d{2})|(?:\d{1,2})))|` +
	`(?:(?:[0-9A-Fa-f]{0,4}:){0,5}:(?:(?:(?:25[0-5])|(?:2[0-4]\d)|(?:1\d{2})|` +
	`(?:\d{1,2}))\.){3}(?:(?:25[0-5])|(?:2[0-4]\d)|(?:1\d{2})|(?:\d{1,2})))|` +
	`(?:::(?:[0-9A-Fa-f]{0,4}:){0,5}(?:(?:(?:25[0-5])|(?:2[0-4]\d)|(?:1\d{2})|` +
	`(?:\d{1,2}))\.){3}(?:(?:25[0-5])|(?:2[0-4]\d)|(?:1\d{2})|(?:\d{1,2})))|` +
	`(?:[0-9A-Fa-f]{0,4}::(?:[0-9A-Fa-f]{0,4}:){0,5}[0-9A-Fa-f]{0,4})|` +
	`(?:::(?:[0-9A-Fa-f]{0,4}:){0,6}[0-9A-Fa-f]{0,4})|(?:(?:[0-9A-Fa-f]{0,4}:){1,7}:)))`

// a global regex to match all IP addresses.
var _IPRegex = regexp.MustCompile(ipv4v6Regex) // nolint:gochecknoglobals

func isIP(raw string) bool {
	return _IPRegex.MatchString(raw)
}

/* checkAccess checks access permissions for request.
req.URL could actually be one of [URL, Domain, IP].
Order of checking is:
1. URL (as the longest from the three possible).
2. Domain or IP (checked by isIP function which uses regular expression).
*/
func checkAccess(checker LocalChecker, req *SquidRequest) (types.CheckAccessResultGetter, error) {
	// Parse URL.
	_URL, err := url.Parse(req.URL)
	if err != nil {
		return nil, err
	}

	// Check Full URL.
	if _URL.Path != "" {
		// nolint:govet
		// shadow of err is OK here.
		res, err := checker.CheckAccessSIPURL(req.ClientIP, _URL)
		if err != nil {
			return nil, fmt.Errorf("can not check Access Permissions for %s to %q: %w", req.ClientIP, _URL, err)
		}

		return res, nil
	}

	// Check Domain/IP.
	hostname := _URL.Hostname()

	// IP.
	if isIP(hostname) {
		// nolint:govet
		// shadow of err is OK here.
		res, err := checker.CheckAccessSIPDIP(req.ClientIP, net.ParseIP(hostname))
		if err != nil {
			return nil, fmt.Errorf("can not check Access Permissions for %s to %q: %w", req.ClientIP, _URL, err)
		}

		return res, nil
	}

	// Domain.
	res, err := checker.CheckAccessSIPDomain(req.ClientIP, hostname)
	if err != nil {
		return nil, fmt.Errorf("can not check Access Permissions for %s to %q: %w", req.ClientIP, _URL, err)
	}

	return res, nil
}
