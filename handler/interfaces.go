package handler

import (
	"net"
	"net/url"

	"gitlab.com/accesscontroller/confreader/types"
)

// LocalSIPURLChecker defines an interface which checks a source IP access permissions to a URL.
type LocalSIPURLChecker interface {
	// CheckAccessSIPURL checks a source IP access to a URL.
	CheckAccessSIPURL(srcIP net.IP, u *url.URL) (types.CheckAccessResultGetter, error)
}

// LocalSIPDomainChecker defines an interface which checks a source IP access permissions to a Domain.
type LocalSIPDomainChecker interface {
	// CheckAccessSIPDomain checks a source IP access to a Domain.
	CheckAccessSIPDomain(srcIP net.IP, domain string) (types.CheckAccessResultGetter, error)
}

// LocalSIPIPChecker defines an interface which checks a source IP access permissions to an IP.
type LocalSIPIPChecker interface {
	// CheckAccessSIPDIP checks a source IP access to an IP.
	CheckAccessSIPDIP(srcIP, dstIP net.IP) (types.CheckAccessResultGetter, error)
}

// LocalChecker defines an interface which implements checking Access Permissions.
type LocalChecker interface {
	LocalSIPDomainChecker
	LocalSIPIPChecker
	LocalSIPURLChecker
}

// RedirectURLMaker defines an interface which creates a URL for Squid redirect.
type RedirectURLMaker interface {
	MakeRedirectURL(res types.CheckAccessResultGetter, clientIP net.IP) string
	MakeErrRedirectURL(_URL string, clientIP net.IP, err error) string
}
