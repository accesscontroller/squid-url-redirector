package handler

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.uber.org/zap"
)

type TestMakeResponseSuite struct {
	suite.Suite
}

func (ts *TestMakeResponseSuite) TestHandleCheckError() {
	var handleErr = errors.New("handle check error") // nolint:err113

	var tests = []*struct {
		failPass bool
		name     string
		req      *SquidRequest
		maker    *TestMockRedirectURLMaker
		out      *bytes.Buffer
	}{
		{
			name:     "test 1",
			failPass: true,
			req: &SquidRequest{
				ConcurencyID: 1001,
				ClientFQDN:   "cl1.example.com",
				ClientIP:     net.ParseIP("10.8.0.1"),
				Ident:        "",
				Method:       "CONNECT",
				MyIP:         "10.0.0.1",
				URL:          "http://domain-1.com",
			},
			maker: &TestMockRedirectURLMaker{},
			out:   &bytes.Buffer{},
		},
		{
			name:     "test 2",
			failPass: true,
			req: &SquidRequest{
				ConcurencyID: 1001,
				ClientFQDN:   "cl1.example.com",
				ClientIP:     net.ParseIP("10.8.0.1"),
				Ident:        "",
				Method:       "CONNECT",
				MyIP:         "10.0.0.1",
				URL:          "http://domain-1.com",
			},
			maker: &TestMockRedirectURLMaker{},
			out:   &bytes.Buffer{},
		},
		{
			name:     "test 3",
			failPass: true,
			req: &SquidRequest{
				ConcurencyID: 1001,
				ClientFQDN:   "cl1.example.com",
				ClientIP:     net.ParseIP("10.8.0.1"),
				Ident:        "",
				Method:       "CONNECT",
				MyIP:         "10.0.0.1",
				URL:          "http://domain-1.com",
			},
			maker: &TestMockRedirectURLMaker{},
			out:   &bytes.Buffer{},
		},
	}

	for _, test := range tests {
		chR := &TestMockCheckAccessResult{}

		gotErr := makeResponse(test.req, test.failPass, chR, test.maker, handleErr, test.out, zap.L())

		if !ts.NoErrorf(gotErr, "Fail test %q", test.name) {
			ts.FailNowf("test %q", test.name)
		}

		gotResponse := test.out.String()

		ts.Equalf(fmt.Sprintf("%d OK message=%q\n", test.req.ConcurencyID, handleErr),
			gotResponse, "test %s", test.name)

		test.maker.AssertExpectations(ts.T())
	}
}

func (ts *TestMakeResponseSuite) TestHandleCheckAccessAllowed() {
	var tests = []*struct {
		failPass bool

		name  string
		req   *SquidRequest
		maker *TestMockRedirectURLMaker
		out   *bytes.Buffer

		// Access Check Result.
		accessGroup storage.ResourceNameT
		isAllowed   bool
		user        storage.ResourceNameT
		source      storage.ResourceNameT
	}{
		{
			name:     "test 1",
			failPass: true,
			req: &SquidRequest{
				ConcurencyID: 1001,
				ClientFQDN:   "cl1.example.com",
				ClientIP:     net.ParseIP("10.8.0.1"),
				Ident:        "",
				Method:       "CONNECT",
				MyIP:         "10.0.0.1",
				URL:          "http://domain-1.com",
			},
			maker:       &TestMockRedirectURLMaker{},
			out:         &bytes.Buffer{},
			isAllowed:   true,
			accessGroup: "access group 1",
			source:      "source 1",
			user:        "user 1",
		},
	}

	for _, test := range tests {
		// Configure mock.
		chR := &TestMockCheckAccessResult{}

		chR.On("IsAllowed").Return(test.isAllowed)
		chR.On("GetAccessGroup").Return(&storage.AccessGroupV1{
			Metadata: storage.MetadataV1{
				Name: test.accessGroup,
			},
		})

		gotErr := makeResponse(test.req, test.failPass, chR, test.maker, nil, test.out, zap.L())

		if !ts.NoErrorf(gotErr, "Fail test %q", test.name) {
			ts.FailNowf("test %q", test.name)
		}

		gotResponse := test.out.String()

		ts.Equalf(fmt.Sprintf("%d OK access_group=%q\n", test.req.ConcurencyID, test.accessGroup),
			gotResponse, "test %s", test.name)

		test.maker.AssertExpectations(ts.T())
	}
}

func (ts *TestMakeResponseSuite) TestHandleCheckAccessForbidden() {
	var tests = []*struct {
		failPass bool

		name  string
		req   *SquidRequest
		maker *TestMockRedirectURLMaker
		out   *bytes.Buffer

		// Access Check Result.
		accessGroup storage.ResourceNameT
		isAllowed   bool
		user        storage.ResourceNameT
		source      storage.ResourceNameT
	}{
		{
			name:     "test 1",
			failPass: true,
			req: &SquidRequest{
				ConcurencyID: 1001,
				ClientFQDN:   "cl1.example.com",
				ClientIP:     net.ParseIP("10.8.0.1"),
				Ident:        "",
				Method:       "CONNECT",
				MyIP:         "10.0.0.1",
				URL:          "http://domain-1.com",
			},
			maker:       &TestMockRedirectURLMaker{},
			out:         &bytes.Buffer{},
			isAllowed:   false,
			accessGroup: "access group 1",
			source:      "source 1",
			user:        "user 1",
		},
		{
			name:     "test 2",
			failPass: true,
			req: &SquidRequest{
				ConcurencyID: 1001,
				ClientFQDN:   "cl1.example.com",
				ClientIP:     net.ParseIP("10.8.0.1"),
				Ident:        "user 1",
				Method:       "CONNECT",
				MyIP:         "10.0.0.1",
				URL:          "http://domain-1.com",
			},
			maker:       &TestMockRedirectURLMaker{},
			out:         &bytes.Buffer{},
			isAllowed:   false,
			accessGroup: "access group 1",
			source:      "source 1",
			user:        "user 1",
		},
	}

	for _, test := range tests {
		// Configure mocks.
		chR := &TestMockCheckAccessResult{}

		chR.On("IsAllowed").Return(test.isAllowed)
		chR.On("GetAccessGroup").Return(&storage.AccessGroupV1{
			Metadata: storage.MetadataV1{
				Name: test.accessGroup,
			},
		})

		test.maker.On("MakeRedirectURL", chR, test.req.ClientIP).
			Return(fmt.Sprintf("http://redirect-url/to/%s/%s/%s/%s/%s",
				test.accessGroup, test.req.URL, test.req.ClientIP,
				test.user, test.source))

		gotErr := makeResponse(test.req, test.failPass, chR, test.maker, nil, test.out, zap.L())

		if !ts.NoErrorf(gotErr, "Fail test %q", test.name) {
			ts.FailNowf("test %q", test.name)
		}

		gotResponse := test.out.String()

		ts.Equalf(fmt.Sprintf("%d OK access_group=%q 302:%q\n",
			test.req.ConcurencyID, test.accessGroup,
			fmt.Sprintf("http://redirect-url/to/%s/%s/%s/%s/%s",
				test.accessGroup, test.req.URL, test.req.ClientIP,
				test.user, test.source)),
			gotResponse, "test %s", test.name)

		test.maker.AssertExpectations(ts.T())
	}
}

func TestMakeResponse(t *testing.T) {
	suite.Run(t, &TestMakeResponseSuite{})
}
