package handler

import (
	"fmt"
	"io"

	"github.com/gofrs/uuid"
	"gitlab.com/accesscontroller/confreader/types"
	"go.uber.org/zap"
)

type checker func(req *SquidRequest) (types.CheckAccessResultGetter, error)

// handleRequest reads and parses one request from io.Reader and writes to io.Writer.
func handleRequest(in string, out io.Writer, failPass bool, chk checker, mkr RedirectURLMaker, logger *zap.Logger) error {
	// Init logger.
	reqIntID, err := uuid.NewV4()
	if err != nil {
		return fmt.Errorf("can not generate Internal Request ID: %w", err)
	}

	iLogger := logger.With(
		zap.String("module", "handle_request"),
		zap.String("InternalRequestID", reqIntID.String()))

	// Parse Request.
	squidReq, err := parseRequest(in)
	if err != nil {
		iLogger.Error("Can not parse Squid request", zap.Error(err))

		return nil
	}

	// Debug.
	iLogger.Debug("Parsed Squid request",
		zap.Int64("ConcurencyID", squidReq.ConcurencyID),
		zap.String("URL", squidReq.URL),
		zap.String("ClientIP", squidReq.ClientIP.String()),
		zap.String("ClientFQDN", squidReq.ClientFQDN),
		zap.String("Method", squidReq.Method),
		zap.String("Ident", squidReq.Ident),
		zap.String("MyIP", squidReq.MyIP),
		zap.Uint16("MyPort", squidReq.MyPort),
	)

	// Check.
	result, checkError := chk(squidReq)

	// Debug.
	if checkError == nil {
		user, source := result.GetUserInfo()
		resource, category := result.GetWebResourceInfo()

		iLogger.Debug("Got Squid request access check result",
			zap.String("AccessGroup", string(result.GetAccessGroup().Metadata.Name)),
			zap.String("ExternalUser", string(user+"@"+source)),
			zap.String("WebResource", string(category+"/"+resource)),
			zap.Bool("IsAccessAllowed", result.IsAllowed()),
		)
	} else {
		iLogger.Debug("Got Squid request access check error", zap.Error(checkError))
	}

	// Send response.
	if err := makeResponse(squidReq, failPass, result, mkr, checkError, out, logger); err != nil {
		iLogger.Error("Can not write response", zap.Error(err))

		return err
	}

	return nil
}
