package handler

import (
	"fmt"
	"io"
	"net"

	"gitlab.com/accesscontroller/confreader/types"
	"go.uber.org/zap"
)

func makeCheckErrResponse(failPass bool, concurencyID int64,
	requestURL string, clientIP net.IP, mkr RedirectURLMaker,
	checkErr error) string {
	// If failPass == true - do not block the request.
	if failPass {
		return fmt.Sprintf("%d ERR message=%q\n", concurencyID, checkErr)
	}

	// // Otherways - block.
	return fmt.Sprintf("%d OK message=%q status=302 url=\"%s\"\n",
		concurencyID, checkErr,
		mkr.MakeErrRedirectURL(requestURL, clientIP, checkErr))
}

func makeCheckSuccessResponse(concurencyID int64, clientIP net.IP,
	chRes types.CheckAccessResultGetter, mkr RedirectURLMaker) string {
	// Access allowed.
	if chRes.IsAllowed() {
		return fmt.Sprintf("%d ERR access_group=%q\n", concurencyID,
			chRes.GetAccessGroup().Metadata.Name)
	}

	// Access Denied.
	return fmt.Sprintf("%d OK access_group=%q status=302 url=\"%s\"\n", concurencyID,
		chRes.GetAccessGroup().Metadata.Name, mkr.MakeRedirectURL(chRes, clientIP))
}

func makeResponse(req *SquidRequest, failPass bool,
	chRes types.CheckAccessResultGetter, maker RedirectURLMaker,
	checkErr error, out io.Writer, logger *zap.Logger) error {
	// Handle check error.
	if checkErr != nil {
		// Prepare response.
		squidResponse := makeCheckErrResponse(failPass, req.ConcurencyID, req.URL, req.ClientIP, maker, checkErr)

		// Debug.
		logger.Debug("Prepared Squid response",
			zap.String("check_error", checkErr.Error()),
			zap.Bool("fail_pass", failPass),
			zap.String("response", squidResponse))

		// Write.
		if _, err := out.Write([]byte(squidResponse)); err != nil {
			return fmt.Errorf("can not write response: %w", err)
		}

		return nil
	}

	// Make response.
	squidResponse := makeCheckSuccessResponse(req.ConcurencyID, req.ClientIP, chRes, maker)

	// Debug.
	logger.Debug("Prepared Squid response",
		zap.Bool("access_allowed", chRes.IsAllowed()),
		zap.String("response", squidResponse),
	)

	// Write.
	if _, err := out.Write([]byte(squidResponse)); err != nil {
		return fmt.Errorf("can not write response: %w", err)
	}

	return nil
}
