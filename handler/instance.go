package handler

import (
	"bufio"
	"context"
	"io"
	"sync/atomic"

	"gitlab.com/accesscontroller/confreader/types"
	"go.uber.org/zap"
)

type Instance struct {
	failPass bool

	checker LocalChecker

	isActive atomic.Value

	logger *zap.Logger

	_URLMaker RedirectURLMaker
}

func New(passOnFail bool, checker LocalChecker, urlMaker RedirectURLMaker, logger *zap.Logger) *Instance {
	return &Instance{
		failPass:  passOnFail,
		checker:   checker,
		logger:    logger,
		_URLMaker: urlMaker,
	}
}

func (inst *Instance) ServeContext(ctx context.Context, in io.Reader, out io.Writer) error {
	logger := inst.logger.With(
		zap.String("module", "serve_context"),
	)

	defer logger.Info("Stopped")

	// Init isActive flag.
	inst.isActive.Store(true)

	// Handle stop.
	go func() {
		<-ctx.Done()

		logger.Info("Stopping...")

		inst.isActive.Store(false)
	}()

	// Reader.
	rdr := bufio.NewScanner(in)

	// Log Requests.
	var isLogRequests = logger.Core().Enabled(zap.DebugLevel)

	for rdr.Scan() {
		// Stop on error.
		if err := rdr.Err(); err != nil {
			logger.Error("Can not read a request line", zap.Error(err))

			return err
		}

		// Log request.
		if isLogRequests {
			logger.Debug("Got a request", zap.String("request_text", rdr.Text()))
		}

		// Stop on a Context#Done.
		if !inst.isActive.Load().(bool) {
			logger.Info("Got Context#Done. Stopping")

			return nil
		}

		// Handle.
		if err := handleRequest(rdr.Text(), out,
			inst.failPass,
			func(req *SquidRequest) (types.CheckAccessResultGetter, error) {
				return checkAccess(inst.checker, req)
			},
			inst._URLMaker, logger); err != nil {
			logger.Error("Handle error. Stop serving", zap.Error(err))

			return err
		}
	}

	logger.Info("Exit on io.Reader EOF")

	return nil
}
