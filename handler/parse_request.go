package handler

import (
	"errors"
	"fmt"
	"net"
	"strconv"
	"strings"
)

var ErrParseSquidRequest = errors.New("can not parse request")

type SquidRequest struct {
	ConcurencyID int64
	URL          string
	ClientIP     net.IP
	ClientFQDN   string
	Method       string
	Ident        string
	MyIP         string
	MyPort       uint16
}

func parseSquidKV(s string) (value string, _ error) {
	kv := strings.Split(s, "=")
	// Format must be key=value so the length is 2.
	if len(kv) != 2 { // nolint:gomnd
		return "", fmt.Errorf("%w: can not parse %q to a key and value pair", ErrParseSquidRequest, s)
	}

	return kv[1], nil
}

func parseRequest(in string) (*SquidRequest, error) {
	// Format:
	// 123 arc.msn.com:443 192.168.0.153/192.168.0.153 - CONNECT myip=- myport=3130 .
	var (
		req       SquidRequest
		IPFQDNRaw string
		myIPRaw   string
		myPortRaw string
		ident     string
	)

	// Request.
	if _, err := fmt.Sscanln(in,
		&req.ConcurencyID,
		&req.URL,
		&IPFQDNRaw,
		&ident,
		&req.Method,
		&myIPRaw,
		&myPortRaw); err != nil {
		return nil, err
	}

	// Ident.
	if ident != "-" {
		req.Ident = ident
	}

	// Parse IP and FQDN.
	spl := strings.Split(IPFQDNRaw, "/")
	// Must be exactly 2 parts: 192.168.0.153/192.168.0.153.
	if len(spl) != 2 { // nolint:gomnd
		return nil, fmt.Errorf("%w: can not parse IP/FQDN from %q", ErrParseSquidRequest, IPFQDNRaw)
	}

	addr := net.ParseIP(spl[0])
	if addr == nil {
		return nil, fmt.Errorf("%w: can not parse IP from %q", ErrParseSquidRequest, spl[0])
	}

	req.ClientIP = addr
	req.ClientFQDN = spl[1]

	// Parse MyIP.
	myIP, err := parseSquidKV(myIPRaw)
	if err != nil {
		return nil, err
	}

	req.MyIP = myIP

	// Parse MyPort.
	myPort, err := parseSquidKV(myPortRaw)
	if err != nil {
		return nil, err
	}

	port, err := strconv.Atoi(myPort)
	if err != nil {
		return nil, fmt.Errorf("%w: can not convert %q to uint16 - port", err, myPort)
	}

	req.MyPort = uint16(port)

	return &req, nil
}
