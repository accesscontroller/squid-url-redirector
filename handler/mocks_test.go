package handler

import (
	"net"

	"github.com/stretchr/testify/mock"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/confreader/types"
)

type TestMockRedirectURLMaker struct {
	mock.Mock
}

func (m *TestMockRedirectURLMaker) MakeRedirectURL(res types.CheckAccessResultGetter, clientIP net.IP) string {
	return m.Called(res, clientIP).String(0)
}

func (m *TestMockRedirectURLMaker) MakeErrRedirectURL(_URL string, clientIP net.IP, err error) string {
	return m.Called(_URL, clientIP, err).String(0)
}

type TestMockCheckAccessResult struct {
	mock.Mock
}

// IsAllowed returns access check result.
func (m *TestMockCheckAccessResult) IsAllowed() bool {
	return m.Called().Bool(0)
}

// GetWebResourceInfo returns a WebResourceV1 info.
func (m *TestMockCheckAccessResult) GetWebResourceInfo() (resource, category storage.ResourceNameT) {
	callRes := m.Called()

	return callRes.Get(0).(storage.ResourceNameT), callRes.Get(1).(storage.ResourceNameT)
}

// GetUserInfo returns an ExternalUserV1 info.
func (m *TestMockCheckAccessResult) GetUserInfo() (user, source storage.ResourceNameT) {
	callRes := m.Called()

	return callRes.Get(0).(storage.ResourceNameT), callRes.Get(1).(storage.ResourceNameT)
}

// GetAccessGroup returns an AccessGroupV1 which enforced the CheckResult.
func (m *TestMockCheckAccessResult) GetAccessGroup() *storage.AccessGroupV1 {
	callRes := m.Called()

	return callRes.Get(0).(*storage.AccessGroupV1)
}
