{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "squid-url-redirector.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 40 chars because some Kubernetes name fields 
are limited to 63 chars (by the DNS naming spec)
and because we append suffixes in resource templates.
If release name contains chart name it will be used as a full name.
*/}}
{{- define "squid-url-redirector.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 40 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 40 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 40 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Define a stub service hostname for usage in Ingress, Service etc.
*/}}
{{- define "squid-url-redirector.fullname.stub" -}}
{{ include "squid-url-redirector.fullname" . }}-stub
{{- end }}


{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "squid-url-redirector.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "squid-url-redirector.labels" -}}
helm.sh/chart: {{ include "squid-url-redirector.chart" . }}
{{ include "squid-url-redirector.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "squid-url-redirector.selectorLabels" -}}
app.kubernetes.io/name: {{ include "squid-url-redirector.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "squid-url-redirector.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "squid-url-redirector.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Defines a configuration for Squid URL Redirector plugin
which is further used as a Secret.
*/}}
{{- define "squid-url-redirector.secret" -}}
pass_all: {{ .Values.apiServer.passAll }}
fail_over_pass: {{ .Values.apiServer.failOverPass }}
log_output: "/var/log/squid/redirector.log"
log_level: {{ .Values.apiServer.logLevel }}
stub_server:
  redirect_url: "{{ .Values.stubServer.redirectURL }}"
conf_reader:
  db_path: {{ .Values.apiServer.confReader.dbPath }}
  sessions_sources:
    {{- range .Values.apiServer.confReader.sessionsSources -}}
    - {{ . | quote | nindent 4 }}
    {{- end -}}
  users_groups_sources: 
    {{- range .Values.apiServer.confReader.usersGroupsSources }}
    - {{ . | quote | nindent 4 }}
    {{- end }}
  timers:
    access_groups_reload: {{ .Values.apiServer.confReader.timers.accessGroupsReload }}
    full_reload: {{ .Values.apiServer.confReader.timers.fullReload }}
    sessions_reload: {{ .Values.apiServer.confReader.timers.sessionsReload }}
    users_groups_reload: {{ .Values.apiServer.confReader.timers.usersGroupsReload }}
    web_resources_reload: {{ .Values.apiServer.confReader.timers.webResourcesReload }}
api_server:
  server: {{ .Values.apiServer.apiServer.server }}
  access_key: {{ .Values.apiServer.apiServer.accessKey }}
  ca_certs: |
    {{- .Values.apiServer.apiServer.caCerts | nindent 4 }}
  client:
    certificate: |
      {{- .Values.apiServer.apiServer.client.certificate | nindent 6 }}
    key: |
      {{- .Values.apiServer.apiServer.client.key | nindent 6 }}
{{- end }}